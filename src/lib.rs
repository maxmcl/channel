use std::collections::VecDeque;
use std::sync::{atomic, Arc, Condvar, Mutex};

pub struct Sender<T> {
    shared: Arc<Shared<T>>,
}

impl<T> Clone for Sender<T> {
    fn clone(&self) -> Self {
        self.shared.senders.fetch_add(1, atomic::Ordering::SeqCst);
        Sender {
            shared: Arc::clone(&self.shared),
        }
    }
}

impl<T> Drop for Sender<T> {
    fn drop(&mut self) {
        let previous = self.shared.senders.fetch_sub(1, atomic::Ordering::SeqCst);
        if previous == 1 {
            self.shared.available.notify_one();
        }
    }
}

impl<T> Sender<T> {
    pub fn send(&mut self, t: T) {
        {
            let mut queue = self.shared.queue.lock().unwrap();
            queue.push_back(t);
        }
        self.shared.available.notify_one();
    }
}

pub struct Receiver<T> {
    shared: Arc<Shared<T>>,
    buffer: VecDeque<T>,
}

impl<T> Iterator for Receiver<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        self.recv()
    }
}

impl<T> Receiver<T> {
    pub fn recv(&mut self) -> Option<T> {
        // Check if there's something in the buffer first
        if let Some(t) = self.buffer.pop_front() {
            return Some(t);
        }
        // Wait as long as the queue protected by the mutex is empty
        let mut queue = self
            .shared
            .available
            .wait_while(self.shared.queue.lock().unwrap(), |queue| {
                queue.is_empty() && self.shared.senders.load(atomic::Ordering::SeqCst) > 0
            })
            .unwrap();
        // Store as many payloads as we can at once
        std::mem::swap(&mut self.buffer, &mut queue);
        println!("Buffer size = {}", self.buffer.len());
        self.buffer.pop_front()
    }
}

struct Shared<T> {
    queue: Mutex<VecDeque<T>>,
    senders: atomic::AtomicUsize,
    available: Condvar,
}

pub fn channel<T>() -> (Sender<T>, Receiver<T>) {
    let shared = Arc::new(Shared {
        queue: Mutex::default(),
        senders: atomic::AtomicUsize::new(1),
        available: Condvar::new(),
    });
    (
        Sender {
            shared: shared.clone(),
        },
        Receiver {
            shared,
            buffer: VecDeque::new(),
        },
    )
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ping_pong() {
        let (mut tx, mut rx) = channel::<&str>();
        tx.send("wut");
        assert_eq!(rx.recv(), Some("wut"));
    }

    #[test]
    fn closed() {
        let (tx, mut rx) = channel::<()>();
        drop(tx);
        assert_eq!(rx.recv(), None);
    }

    #[test]
    fn closed_rx() {
        let (mut tx, rx) = channel::<&str>();
        drop(rx);
        tx.send("yo");
    }

    #[test]
    fn iter_and_batch() {
        let (mut tx, rx) = channel::<i32>();
        tx.send(1);
        tx.send(2);
        tx.send(3);
        let mut it = rx.into_iter();
        assert_eq!(Some(1), it.next());
        assert_eq!(Some(2), it.next());
        assert_eq!(Some(3), it.next());
    }
}
